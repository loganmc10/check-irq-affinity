#!/usr/bin/env python3
import argparse
import time
import json
from typing import Dict, List


def main() -> None:
    smp_affinity: Dict[str, List[str]] = {}
    parser = argparse.ArgumentParser(
        description="Check interrupt affinity based on interface"
    )
    parser.add_argument(
        "-i",
        "--interfaces",
        type=str,
        help="a space separated list of interfaces",
        nargs="+",
        required=True,
    )
    parser.add_argument(
        "-t",
        "--time",
        type=int,
        help="time to run check for, in seconds. Defaults to 60",
        default=60,
    )

    args = parser.parse_args()
    for x in range(args.time):
        with open("/proc/interrupts") as f:
            for line in f.readlines():
                for interface in args.interfaces:
                    if interface in line:
                        irq_number = line.split()[0].strip(":")
                        with open(f"/proc/irq/{irq_number}/smp_affinity_list") as f2:
                            irq_affinity = f2.read().strip()
                            if interface not in smp_affinity:
                                smp_affinity[interface] = []
                            if irq_affinity not in smp_affinity[interface]:
                                smp_affinity[interface].append(irq_affinity)
                                smp_affinity[interface].sort()
        time.sleep(1)
    print(json.dumps(smp_affinity, indent=4))


if __name__ == "__main__":
    main()
